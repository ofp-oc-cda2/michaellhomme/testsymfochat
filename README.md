# Mise en place de l'environnement

`symfony console doctrine:database:create`

`symfony console doctrine:migrations:migrate`

`symfony console doctrine:fixtures:load`
