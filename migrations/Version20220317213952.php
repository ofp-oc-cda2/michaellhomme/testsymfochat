<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220317213952 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE discussion (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, user_a_id INTEGER NOT NULL, user_b_id INTEGER NOT NULL)');
        $this->addSql('CREATE INDEX IDX_C0B9F90F415F1F91 ON discussion (user_a_id)');
        $this->addSql('CREATE INDEX IDX_C0B9F90F53EAB07F ON discussion (user_b_id)');
        $this->addSql('CREATE TABLE message (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, discussion_id INTEGER NOT NULL, user_from_id INTEGER NOT NULL, user_to_id INTEGER NOT NULL, message VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE INDEX IDX_B6BD307F1ADED311 ON message (discussion_id)');
        $this->addSql('CREATE INDEX IDX_B6BD307F20C3C701 ON message (user_from_id)');
        $this->addSql('CREATE INDEX IDX_B6BD307FD2F7B13D ON message (user_to_id)');
        $this->addSql('CREATE TABLE "user" (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, username VARCHAR(180) NOT NULL, roles CLOB NOT NULL --(DC2Type:json)
        , password VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649F85E0677 ON "user" (username)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE discussion');
        $this->addSql('DROP TABLE message');
        $this->addSql('DROP TABLE "user"');
    }
}
