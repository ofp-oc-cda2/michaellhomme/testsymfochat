<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;


use App\Entity\User;
use App\Entity\Message;
use App\Entity\Discussion;

class AppFixtures extends Fixture
{

    private UserPasswordHasherInterface $hasher;

    public function __construct(UserPasswordHasherInterface $hasher)
    {
        $this->hasher = $hasher;
    }

    public function load(ObjectManager $em): void
    {
        $userBob = $this->createUser($em, "bob");
        $userAlice = $this->createUser($em, "alice");
        $userRemy = $this->createUser($em, "remy");

        // Flush to populate users IDs
        $em->flush();

        $this->createDiscussion($em, $userBob, $userAlice);
        $this->createDiscussion($em, $userAlice, $userRemy);

        $em->flush();
    }

    private function createUser(ObjectManager $em, string $username): User
    {
        $user = new User();
        $user->setUsername($username);
        $user->setPassword($this->hasher->hashPassword($user, 'pass_1234'));
        $em->persist($user);
        return $user;
    }

    private function createDiscussion(ObjectManager $em, User $userA, User $userB): void
    {
        $discussion = new Discussion();

        if($userA->getId() < $userB->getId()) {
          $discussion->setUserA($userA);
          $discussion->setUserB($userB);
        } else {
          $discussion->setUserA($userB);
          $discussion->setUserB($userA);
        }

        // Persist and flush to populate discussion ID
        $em->persist($discussion);
        $em->flush();

        $this->createMessage($em, $discussion, $userA, $userB, "Hello how are you ?");
        $this->createMessage($em, $discussion, $userB, $userA, "Fine and you ?");
        $this->createMessage($em, $discussion, $userA, $userB, "Never better");

        // Persist messages
        $em->flush();
    }

    private function createMessage(ObjectManager $em, Discussion $d, User $userFrom, User $userTo, string $msg): void
    {
      $m = new Message();
      $m->setDiscussion($d);
      $m->setUserTo($userTo);
      $m->setUserFrom($userFrom);
      $m->setMessage($msg);

      $em->persist($m);
    }

}
